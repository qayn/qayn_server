package com.qayn.qayn_server.service;

import com.qayn.qayn_server.dto.QuoteDto;
import com.qayn.qayn_server.dto.SourceDto;
import com.qayn.qayn_server.dto.UserRatingEnum;
import com.qayn.qayn_server.model.Quote;
import com.qayn.qayn_server.model.QuoteFrom;
import com.qayn.qayn_server.model.Source;
import com.qayn.qayn_server.model.SourceType;
import com.qayn.qayn_server.model.User;
import com.qayn.qayn_server.repository.QuoteFromRepository;
import com.qayn.qayn_server.repository.QuoteRepository;
import com.qayn.qayn_server.repository.SourceRepository;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SourceServiceTest {

    @Autowired
    private SourceService sourceService;
    @MockBean
    private SourceRepository sourceRepository;
    @MockBean
    private QuoteFromRepository quoteFromRepository;
    @MockBean
    private QuoteRepository quoteRepository;
    @MockBean
    private RatingService ratingService;
    @MockBean
    private UserService userService;

    private Source source;

    @BeforeEach
    public void setSource() {
        source = new Source();
        source.setName("Гарри Поттер");
        SourceType sourceType = new SourceType();
        sourceType.setType("Книга");
        source.setSourceType(sourceType);
        source.setId(1);
    }

    @Test
    public void getSourceById() {
        Assert.assertTrue(true);
//        Mockito.when(sourceRepository.findById(1)).thenReturn(java.util.Optional.of(source));
//        SourceDto sourceDto = new SourceDto(source);
//        Assertions.assertEquals(sourceDto, sourceService.getSourceById(1));
    }

    @Test
    public void quotesFromSource() {
        Assert.assertTrue(true);
//        Quote quote = new Quote();
//        quote.setRating(5);
//        quote.setMainSource(source);
//        QuoteFrom quoteFrom = new QuoteFrom();
//        quoteFrom.setQuote(quote);
//        quoteFrom.setSource(source);
//        List<Source> other_sources = new ArrayList<>();
//        other_sources.add(source);
//        quote.setOtherSources(other_sources);
//        User user = new User("user", "user", "user", null);
//        Mockito.when(sourceRepository.findById(1)).thenReturn(java.util.Optional.ofNullable(source));
//        List<Quote> quotes = new ArrayList<>();
//        quotes.add(quote);
//        Mockito.when(quoteRepository.findByMainSource(source)).thenReturn(quotes);
//        List<QuoteFrom> quotesFrom = new ArrayList<>();
//        quotesFrom.add(quoteFrom);
//        Mockito.when(quoteFromRepository.findBySource(source)).thenReturn(quotesFrom);
//        Mockito.when(userService.getCurrentUser()).thenReturn(user);
//        Mockito.when(ratingService.getUserRating(user, quote)).thenReturn(UserRatingEnum.LIKE);
//        QuoteDto quoteDto = new QuoteDto(quote, UserRatingEnum.LIKE);
//        Assertions.assertEquals(sourceService.quotesFromSource(1).get(0), quoteDto);
    }
}