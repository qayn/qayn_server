package com.qayn.qayn_server.service;

import com.qayn.qayn_server.dto.RegistrationRequestDto;
import com.qayn.qayn_server.model.User;
import com.qayn.qayn_server.model.UserRole;
import com.qayn.qayn_server.repository.UserRepository;
import com.qayn.qayn_server.repository.UserRoleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {
    @Autowired
    private UserService userService;
    @MockBean
    private UserRepository userRepository;
    @MockBean
    private UserRoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Test
    public void register() {
        userService = new UserService(userRepository, roleRepository, passwordEncoder);
        RegistrationRequestDto newUser = new RegistrationRequestDto();
        UserRole role = new UserRole();
        role.setRoleName("Пользователь");
        role.setId(2);
        newUser.setPassword("password");
        newUser.setLogin("123user");
        newUser.setEmail("123user@123user.123user");
        User repositoryUser = new User();
        repositoryUser.setPasswordHash(passwordEncoder.encode("password"));
        repositoryUser.setRole(role);
        repositoryUser.setUsername("123user");
        repositoryUser.setEmail("123user@123user.123user");

        Mockito.when(userRepository.save(Mockito.any())).thenReturn(repositoryUser);

        User actualUser = userService.register(newUser);
        Assertions.assertEquals(repositoryUser, actualUser);
    }

    @Test
    public void getAll() {
        List<User> users = new ArrayList<>();
        User reposUser = new User();
        UserRole role = new UserRole();
        role.setId(2);
        role.setRoleName("Пользователь");
        reposUser.setRole(role);
        reposUser.setEmail("useruser");
        reposUser.setUsername("useruser");
        reposUser.setPasswordHash("useruser");
        reposUser.setId(8);
        users.add(reposUser);
        Mockito.when(userRepository.findAll()).thenReturn(users);
        List<User> userList = userService.getAll();
        Assertions.assertEquals(userList, users);
    }

    @Test
    public void findByUsername() {
        List<User> users = new ArrayList<>();
        User reposUser = new User();
        UserRole role = new UserRole();
        role.setId(2);
        role.setRoleName("Пользователь");
        reposUser.setRole(role);
        reposUser.setEmail("useruser");
        reposUser.setUsername("useruser");
        reposUser.setPasswordHash("useruser");
        reposUser.setId(8);
        Mockito.when(userRepository.findByUsername("useruser")).thenReturn(java.util.Optional.of(reposUser));
        User user = userService.findByUsername("useruser");
        Assertions.assertEquals(reposUser, user);

        user = userService.findByUsername("useruseruser");
        Assertions.assertNull(user);
    }

    @Test
    public void isEmailUnique() {
        String notUniqueEmail = "useruser";
        String uniqueEmail = "user";
        User user = new User("useruser", "useruser", "useruser", null);
        Mockito.when(userRepository.findByEmail(notUniqueEmail)).thenReturn(java.util.Optional.of(user));
        Assertions.assertTrue(userService.isEmailUnique(uniqueEmail));
        Assertions.assertFalse(userService.isEmailUnique(notUniqueEmail));
    }

    @Test
    public void isLoginUnique() {
        String notUniqueLogin = "useruser";
        String uniqueLogin = "user";
        User user = new User("useruser", "useruser", "useruser", null);
        Mockito.when(userRepository.findByUsername(notUniqueLogin)).thenReturn(java.util.Optional.of(user));
        Assertions.assertTrue(userService.isLoginUnique(uniqueLogin));
        Assertions.assertFalse(userService.isLoginUnique(notUniqueLogin));
    }

}