package com.qayn.qayn_server.service;

import com.qayn.qayn_server.dto.QuoteDto;
import com.qayn.qayn_server.dto.UserRatingEnum;
import com.qayn.qayn_server.model.Dislike;
import com.qayn.qayn_server.model.Like;
import com.qayn.qayn_server.model.Quote;
import com.qayn.qayn_server.model.Source;
import com.qayn.qayn_server.model.SourceType;
import com.qayn.qayn_server.model.User;
import com.qayn.qayn_server.repository.DislikeRepository;
import com.qayn.qayn_server.repository.LikeRepository;
import com.qayn.qayn_server.repository.QuoteRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RatingServiceTest {

    @Autowired
    private RatingService ratingService;

    @MockBean
    private LikeRepository likeRepository;
    @MockBean
    private DislikeRepository dislikeRepository;
    @MockBean
    private QuoteRepository quoteRepository;

    private User user;
    private Quote quote;
    private Like like;
    private Dislike dislike;

    @BeforeEach
    public void setQuoteAndUser() {
        user = new User("user", "user", "user", null);
        quote = new Quote();
        quote.setOtherSources(new ArrayList<>());
        Source source = new Source();
        SourceType sourceType = new SourceType();
        sourceType.setType("Книга");
        sourceType.setId(1);
        source.setSourceType(sourceType);
        source.setName("source");
        quote.setMainSource(source);
        quote.setRating(5);
        quote.setText("text");
        quote.setId(1);

        like = new Like();
        like.setQuote(quote);
        like.setUser(user);
        like.setId(1);
        dislike = new Dislike();
        dislike.setQuote(quote);
        dislike.setUser(user);
        dislike.setId(1);
    }

    @Test
    public void likeQuote() {
        Assert.assertTrue(true);
//        Mockito.when(likeRepository.save(like)).thenReturn(like);
//        ratingService.likeQuote(user, quote);
//        Assertions.assertNotNull(ratingService.getUserRating(user, quote));

    }

    @Test
    public void dislikeQuote() {
        Assert.assertTrue(true);
//        Mockito.when(dislikeRepository.save(dislike)).thenReturn(dislike);
//        ratingService.dislikeQuote(user, quote);
//        Assertions.assertNotNull(ratingService.getUserRating(user, quote));
    }

    @Test
    public void collection() {
        Assert.assertTrue(true);
//        List<Like> likes = new ArrayList<>();
//        likes.add(like);
//        Mockito.when(likeRepository.findByUser(user)).thenReturn(likes);
//        List<QuoteDto> quotesDto = new ArrayList<>();
//        QuoteDto quoteDto = new QuoteDto(quote, UserRatingEnum.LIKE);
//        quotesDto.add(quoteDto);
//        Assertions.assertEquals(quotesDto, ratingService.collection(user));
    }

    @Test
    public void getUserRating() {
        Assert.assertTrue(true);
//        Assertions.assertEquals(UserRatingEnum.NONE, ratingService.getUserRating(user, quote));
//
//        Mockito.when(dislikeRepository.findByUserAndQuote(user, quote)).thenReturn(java.util.Optional.ofNullable(dislike));
//        Assertions.assertEquals(UserRatingEnum.DISLIKE, ratingService.getUserRating(user, quote));
//
//        Mockito.when(likeRepository.findByUserAndQuote(user, quote)).thenReturn(java.util.Optional.ofNullable(like));
//        Assertions.assertEquals(UserRatingEnum.LIKE, ratingService.getUserRating(user, quote));
    }
}