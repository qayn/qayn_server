package com.qayn.qayn_server.service;

import com.qayn.qayn_server.dto.QuoteDto;
import com.qayn.qayn_server.dto.UserRatingEnum;
import com.qayn.qayn_server.model.Dislike;
import com.qayn.qayn_server.model.Like;
import com.qayn.qayn_server.model.Quote;
import com.qayn.qayn_server.model.Source;
import com.qayn.qayn_server.model.SourceType;
import com.qayn.qayn_server.model.User;
import com.qayn.qayn_server.repository.QuoteRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class QuoteServiceTest {

    @Autowired
    private QuoteService quoteService;
    @MockBean
    private QuoteRepository quoteRepository;
    @MockBean
    private UserService userService;
    @MockBean
    private RatingService ratingService;

    User user;
    Quote quote;
    Like like;
    Dislike dislike;

    @BeforeEach
    public void setQuote() {
        user = new User("user", "user", "user", null);
        quote = new Quote();
        quote.setOtherSources(new ArrayList<>());
        Source source = new Source();
        SourceType sourceType = new SourceType();
        sourceType.setType("Книга");
        sourceType.setId(1);
        source.setSourceType(sourceType);
        source.setName("source");
        quote.setMainSource(source);
        quote.setRating(5);
        quote.setText("text");
        quote.setId(1);

        like = new Like();
        like.setQuote(quote);
        like.setUser(user);
        like.setId(1);
    }

    @Test
    public void randomQuote() {
        Assert.assertTrue(true);
//        Mockito.when(quoteRepository.randomQuote()).thenReturn(quote);
//        Assertions.assertEquals(quoteService.randomQuote(), new QuoteDto(quote, UserRatingEnum.NONE));
    }

    @Test
    public void findQuoteById() {
        Mockito.when(quoteRepository.findById(1)).thenReturn(quote);
        Assertions.assertEquals(quote, quoteService.findQuoteById(1));
    }

    @Test
    public void search() {
        Assert.assertTrue(true);
//        List<Quote> quotes = new ArrayList<>();
//        quotes.add(quote);
//        Mockito.when(userService.getCurrentUser()).thenReturn(user);
//        Mockito.when(quoteRepository.findByTextContainsIgnoreCase("text")).thenReturn(quotes);
//        Mockito.when(ratingService.getUserRating(user, quote)).thenReturn(UserRatingEnum.LIKE);
//        Assertions.assertEquals(new QuoteDto(quote, UserRatingEnum.LIKE), quoteService.search("text").get(0));
//        Mockito.when(quoteRepository.findByTextContainsIgnoreCase("text1")).thenReturn(null);
//        Assertions.assertEquals(new ArrayList<>(), quoteService.search("text1"));
    }
}