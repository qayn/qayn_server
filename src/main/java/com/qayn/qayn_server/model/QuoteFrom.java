package com.qayn.qayn_server.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "quotes_from")
public class QuoteFrom {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "quote_from_id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "quote_id")
    private Quote quote;

    @ManyToOne
    @JoinColumn(name = "source_id")
    private Source source;
}
