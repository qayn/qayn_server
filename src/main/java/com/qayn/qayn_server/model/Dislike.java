package com.qayn.qayn_server.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "disliked_quotes")
public class Dislike {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "disliked_quote_id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "quote_id")
    private Quote quote;
}
