package com.qayn.qayn_server.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "quotes")
public class Quote implements Comparable<Quote> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "quote_id")
    private int id;

    @Column(name = "quote")
    private String text;

    @ManyToOne
    @JoinColumn(name = "main_source_id", referencedColumnName = "source_id")
    private Source mainSource;

    @ManyToMany
    @JoinTable(name = "quotes_from",
        joinColumns = @JoinColumn(name = "quote_id"),
        inverseJoinColumns = @JoinColumn(name = "source_id"))
    private List<Source> otherSources;

    @Column(name = "rating")
    private Integer rating;

    @Override
    public int compareTo(Quote o) {
        return o.getRating().compareTo(this.getRating());
    }
}
