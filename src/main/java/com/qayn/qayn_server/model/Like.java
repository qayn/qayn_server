package com.qayn.qayn_server.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "liked_quotes")
public class Like {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "liked_quote_id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "quote_id")
    private Quote quote;
}
