package com.qayn.qayn_server.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "sources")
public class Source {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "source_id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "source_type_id")
    private SourceType sourceType;

    @Column(name = "source_name")
    private String name;
}
