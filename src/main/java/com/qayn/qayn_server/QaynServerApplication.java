package com.qayn.qayn_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QaynServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(QaynServerApplication.class, args);
	}

}
