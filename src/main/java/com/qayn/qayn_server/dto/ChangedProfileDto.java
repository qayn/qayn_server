package com.qayn.qayn_server.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
public class ChangedProfileDto {
    @NotBlank
    private String login;
    @NotBlank
    private String email;
}
