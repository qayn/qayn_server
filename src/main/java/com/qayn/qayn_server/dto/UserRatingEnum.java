package com.qayn.qayn_server.dto;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum UserRatingEnum {
    LIKE("like"),
    DISLIKE("dislike"),
    NONE("none");

    @JsonValue
    private final String userRating;
}
