package com.qayn.qayn_server.dto;

import com.qayn.qayn_server.model.Source;
import lombok.Data;

@Data
public class SourceDto {
    private int id;
    private String name;
    private String type;

    public SourceDto(Source source) {
        this.id = source.getId();
        this.name = source.getName();
        this.type = source.getSourceType().getType();
    }
}
