package com.qayn.qayn_server.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.qayn.qayn_server.model.User;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDto {
    private Integer id;
    private String login;
    private String email;
    //private String password;

    public static UserDto fromUser(User user) {
        UserDto userDto = new UserDto();
        userDto.login = user.getUsername();
        userDto.email = user.getEmail();
        userDto.id = user.getId();

        return userDto;
    }
}
