package com.qayn.qayn_server.dto;

import com.qayn.qayn_server.model.Quote;
import com.qayn.qayn_server.model.Source;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class QuoteDto {
    private int id;
    private String text;
    private SourceDto mainSource;
    private List<SourceDto> otherSources;
    private UserRatingEnum userRating;

    public QuoteDto(Quote quote, UserRatingEnum userRating) {
        this.id = quote.getId();
        this.text = quote.getText();
        this.mainSource = new SourceDto(quote.getMainSource());
        this.otherSources = new ArrayList<>();
        for (Source source : quote.getOtherSources()) {
            otherSources.add(new SourceDto(source));
        }
        this.userRating = userRating;
    }
}
