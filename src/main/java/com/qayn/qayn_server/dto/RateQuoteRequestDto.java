package com.qayn.qayn_server.dto;

import lombok.Data;

@Data
public class RateQuoteRequestDto {
    private int quoteId;
    private Boolean like;
}
