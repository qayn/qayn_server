package com.qayn.qayn_server.api;

public class Messages {

    public static final String EMAIL_NOT_UNIQUE = "Данный email уже используется";
    public static final String LOGIN_NOT_UNIQUE = "Данный логин уже используется";
    public static final String LOGIN_IS_EMPTY = "Поле 'Логин' пустое";
    public static final String PASSWORD_IS_EMPTY = "Поле 'Пароль' пустое";
    public static final String EMAIL_IS_EMPTY = "Поле 'Почта' пустое";
    public static final String UPDATE_PROFILE_SUCCESS = "Данные успешно обновлены";
    public static final String INCORRECT_EMAIL = "Недопустимая почта";
    public static final String INCORRECT_LOGIN = "Недопустимый логин";
    public static final String UPDATE_PASSWORD_SUCCESS = "Пароль обновлён успешно";
    public static final String INCORRECT_NEW_PASSWORD = "Недопустимый пароль";
    public static final String INCORRECT_PASSWORD = "Неверный текущий пароль";

    public static final String USER_NOT_FOUND = "Неверные логин/пароль";
    public static final String REGISTER_SUCCESS = "Регистрация прошла успешно";

    public static final String USER_NOT_AUTHORIZED = "Не удалось идентифицировать пользователя";

    public static final String QUOTE_RATED = "Оценка поставлена";

    public static final String SOURCE_NOT_FOUND = "Источника с данным id не существует";
}
