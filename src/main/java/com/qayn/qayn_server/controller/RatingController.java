package com.qayn.qayn_server.controller;

import com.qayn.qayn_server.api.Messages;
import com.qayn.qayn_server.api.ResponseMessage;
import com.qayn.qayn_server.dto.QuoteDto;
import com.qayn.qayn_server.model.Quote;
import com.qayn.qayn_server.model.User;
import com.qayn.qayn_server.service.QuoteService;
import com.qayn.qayn_server.service.RatingService;
import com.qayn.qayn_server.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/")
@RequiredArgsConstructor
@Slf4j
public class RatingController {

    private final QuoteService quoteService;

    private final UserService userService;

    private final RatingService ratingService;

    @PostMapping(value = "/rateQuote")
    public ResponseEntity<?> rateQuote(@RequestParam Integer quoteId, @RequestParam Optional<Boolean> like) {
        log.info("handling /rateQuote request");
        Quote quote = quoteService.findQuoteById(quoteId);
        User user;
        try {
            user = userService.getCurrentUser();
        } catch (ResponseStatusException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(Messages.USER_NOT_AUTHORIZED);
        }


        ratingService.removeCurrentRatingIfExists(user, quote);

        if (like.isPresent()) {
            if (like.get()) {
                ratingService.likeQuote(user, quote);
            }
            else {
                ratingService.dislikeQuote(user, quote);
            }
        }

        return ResponseEntity.ok(new ResponseMessage(Messages.QUOTE_RATED));
    }

    @GetMapping(value = "/myCollection")
    public List<QuoteDto> collection() {
        User user = userService.getCurrentUser();
        return ratingService.collection(user);
    }
}
