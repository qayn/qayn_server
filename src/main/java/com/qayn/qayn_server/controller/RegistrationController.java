package com.qayn.qayn_server.controller;


import com.qayn.qayn_server.api.Messages;
import com.qayn.qayn_server.api.ResponseMessage;
import com.qayn.qayn_server.dto.RegistrationRequestDto;
import com.qayn.qayn_server.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(value = "/api/")
@RequiredArgsConstructor
public class RegistrationController {

    private final UserService userService;

    @PostMapping("/register")
    public ResponseEntity<ResponseMessage> register(@RequestBody RegistrationRequestDto registrationRequestDto) {
        try {
            userService.register(registrationRequestDto);
        } catch (ResponseStatusException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(e.getReason()));
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseMessage(Messages.REGISTER_SUCCESS));
    }
}
