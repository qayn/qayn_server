package com.qayn.qayn_server.controller;

import com.qayn.qayn_server.api.Messages;
import com.qayn.qayn_server.api.ResponseMessage;
import com.qayn.qayn_server.dto.ChangedPasswordDto;
import com.qayn.qayn_server.dto.ChangedProfileDto;
import com.qayn.qayn_server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(value = "/api")
public class ProfileController {

    private final UserService userService;

    @Autowired
    public ProfileController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/profile")
    public ResponseEntity<?> getMyProfile() {
        try {
            return new ResponseEntity<>(userService.getUserProfile(), HttpStatus.OK);
        } catch (ResponseStatusException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new ResponseMessage(Messages.USER_NOT_AUTHORIZED));
        }
    }

    @PostMapping("/profile/edit")
    public ResponseEntity<?> changeProfile(@RequestBody ChangedProfileDto changedProfileDto) {
        return userService.changeUserProfile(changedProfileDto);
    }

    @PostMapping("/profile/changePassword")
    public ResponseEntity<?> changePassword(@RequestBody ChangedPasswordDto changedPasswordDto) {
        return userService.changePassword(changedPasswordDto);
    }
}
