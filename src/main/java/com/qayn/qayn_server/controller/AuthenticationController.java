package com.qayn.qayn_server.controller;

import com.qayn.qayn_server.api.Messages;
import com.qayn.qayn_server.api.ResponseMessage;
import com.qayn.qayn_server.dto.AuthenticationRequestDto;
import com.qayn.qayn_server.dto.TokenDto;
import com.qayn.qayn_server.model.User;
import com.qayn.qayn_server.security.jwt.JwtTokenProvider;
import com.qayn.qayn_server.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(value = "/api/")
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationManager authenticationManager;

    private final JwtTokenProvider jwtTokenProvider;

    private final UserService userService;

    @PostMapping(value = "/login")
    public ResponseEntity<?> login (@RequestBody AuthenticationRequestDto requestDto) {
        try {
            String username = requestDto.getLogin();
            String password = requestDto.getPassword();
            if (username.isEmpty()) {
                return ResponseEntity.badRequest().body(new ResponseMessage(Messages.LOGIN_IS_EMPTY));
            }
            if (password.isEmpty()){
                return ResponseEntity.badRequest().body(new ResponseMessage(Messages.PASSWORD_IS_EMPTY));
            }
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, requestDto.getPassword()));
            User user = userService.findByUsername(username);

            String token = jwtTokenProvider.createToken(username, user.getRole());

            TokenDto response = new TokenDto(token);

            return ResponseEntity.ok(response);
        } catch (AuthenticationException e) {
            return ResponseEntity.badRequest().body(new ResponseMessage(Messages.USER_NOT_FOUND));
        }
    }
}
