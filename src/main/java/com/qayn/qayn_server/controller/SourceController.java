package com.qayn.qayn_server.controller;

import com.qayn.qayn_server.dto.QuoteDto;
import com.qayn.qayn_server.dto.SourceDto;
import com.qayn.qayn_server.service.SourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/")
public class SourceController {
    @Autowired
    private SourceService sourceService;

    @GetMapping(value = "/source/{id}")
    public SourceDto getSourceById(@PathVariable int id) {
        return sourceService.getSourceById(id);
    }

    @GetMapping(value = "/quotesFromSource/{id}")
    public List<QuoteDto> quotesFromSource(@PathVariable int id) {
        return sourceService.quotesFromSource(id);
    }
}
