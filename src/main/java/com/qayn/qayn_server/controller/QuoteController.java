package com.qayn.qayn_server.controller;

import com.qayn.qayn_server.dto.QuoteDto;
import com.qayn.qayn_server.service.QuoteService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/")
@RequiredArgsConstructor
public class QuoteController {

    private final QuoteService quoteService;

    @GetMapping(value = "/randomQuote")
    public QuoteDto randomQuote() {
        return quoteService.randomQuote();
    }

    @GetMapping(value = "/searchQuotes")
    public List<QuoteDto> search(@RequestParam String query) {
        return quoteService.search(query);
    }
}
