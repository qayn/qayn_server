package com.qayn.qayn_server.repository;

import com.qayn.qayn_server.model.Source;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Transactional
public interface SourceRepository extends JpaRepository<Source, Integer> {
    Optional<Source> findById(int id);
}
