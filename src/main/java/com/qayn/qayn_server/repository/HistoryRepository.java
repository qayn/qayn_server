package com.qayn.qayn_server.repository;

import com.qayn.qayn_server.model.History;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface HistoryRepository extends JpaRepository<History, Integer> {
    List<History> findAll();
}
