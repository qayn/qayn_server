package com.qayn.qayn_server.repository;

import com.qayn.qayn_server.model.Like;
import com.qayn.qayn_server.model.Quote;
import com.qayn.qayn_server.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Transactional
public interface LikeRepository extends JpaRepository<Like, Integer> {
    Optional<Like> findByUserAndQuote(User user, Quote quote);
    List<Like> findByUser(User user);
}
