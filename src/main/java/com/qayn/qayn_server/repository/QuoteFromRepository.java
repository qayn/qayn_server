package com.qayn.qayn_server.repository;

import com.qayn.qayn_server.model.QuoteFrom;
import com.qayn.qayn_server.model.Source;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface QuoteFromRepository extends JpaRepository<QuoteFrom, Integer> {
    List<QuoteFrom> findBySource(Source source);
}
