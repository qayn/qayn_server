package com.qayn.qayn_server.repository;

import com.qayn.qayn_server.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface UserRoleRepository extends JpaRepository <UserRole, Integer> {
    UserRole findById(int id);
    UserRole findByRoleName(String roleName);

}
