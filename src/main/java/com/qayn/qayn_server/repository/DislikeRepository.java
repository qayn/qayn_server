package com.qayn.qayn_server.repository;

import com.qayn.qayn_server.model.Dislike;
import com.qayn.qayn_server.model.Quote;
import com.qayn.qayn_server.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Transactional
public interface DislikeRepository extends JpaRepository<Dislike, Integer> {
    Optional<Dislike> findByUserAndQuote(User user, Quote quote);
}
