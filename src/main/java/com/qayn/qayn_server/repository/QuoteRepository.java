package com.qayn.qayn_server.repository;

import com.qayn.qayn_server.model.Quote;
import com.qayn.qayn_server.model.Source;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface QuoteRepository extends JpaRepository<Quote, Integer> {
    @Query(value = "select * from qayn_schema.quotes order by random() limit 1", nativeQuery = true)
    Quote randomQuote();

    Quote findById(int id);

    @Modifying
    @Query(value = "update Quote quote set quote.rating = ?2 where quote.id = ?1")
    void setRatingById(int id, int newRating);

    List<Quote> findByMainSource(Source source);

    List<Quote> findByTextContainsIgnoreCase(String query);
}
