package com.qayn.qayn_server.service;

import com.qayn.qayn_server.dto.QuoteDto;
import com.qayn.qayn_server.dto.UserRatingEnum;
import com.qayn.qayn_server.model.Quote;
import com.qayn.qayn_server.model.User;
import com.qayn.qayn_server.repository.QuoteRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class QuoteService {
    private final QuoteRepository quoteRepository;
    private final UserService userService;
    private final RatingService ratingService;

    public QuoteDto randomQuote() {
        Quote quote = quoteRepository.randomQuote();
        return new QuoteDto(quote, UserRatingEnum.NONE);
    }

    public Quote findQuoteById(int id) {
        return quoteRepository.findById(id);
    }

    public List<QuoteDto> search(String query) {
        User user = userService.getCurrentUser();
        List<Quote> quotes = quoteRepository.findByTextContainsIgnoreCase(query);
        List<QuoteDto> result = new ArrayList<>();
        if (quotes == null) quotes = new ArrayList<>();
        for (Quote quote : quotes) {
            result.add(new QuoteDto(quote, ratingService.getUserRating(user, quote)));
        }
        return result;
    }
}
