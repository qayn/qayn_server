package com.qayn.qayn_server.service;

import com.qayn.qayn_server.api.Messages;
import com.qayn.qayn_server.api.ResponseMessage;
import com.qayn.qayn_server.dto.ChangedPasswordDto;
import com.qayn.qayn_server.dto.ChangedProfileDto;
import com.qayn.qayn_server.dto.RegistrationRequestDto;
import com.qayn.qayn_server.dto.UserDto;
import com.qayn.qayn_server.model.User;
import com.qayn.qayn_server.model.UserRole;
import com.qayn.qayn_server.repository.UserRepository;
import com.qayn.qayn_server.repository.UserRoleRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {
    private final UserRepository userRepository;
    private final UserRoleRepository userRoleRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    public User register(RegistrationRequestDto registrationRequest) {
        valCheckRegistration(registrationRequest);
        UserRole role = userRoleRepository.findByRoleName("Пользователь");
        User user = new User(
                registrationRequest.getLogin(),
                registrationRequest.getEmail(),
                passwordEncoder.encode(registrationRequest.getPassword()),
                role
        );

        User registeredUser = userRepository.save(user);


        log.info("IN register - user: {} successfully registered", registeredUser);
        return registeredUser;
    }

    public List<User> getAll() {
        List <User> result = userRepository.findAll();
        log.info("IN getAll - {} users found", result.size());
        return result;
    }

    /*public User findById(int id) {
        User result = userRepository.findById(id).orElse(null);
        if (result == null) {
            log.warn("IN findById - no user found by id: {}", id);
        }
        log.info("IN findById - user: {} found by id: {}", result, id);
        return result;
    }*/

    public User findByUsername(String username) {
        User result = userRepository.findByUsername(username).orElse(null);
        log.info("IN findByUsername - user: {} found by username: {}", result, username);
        return result;
    }

    /*public void delete(int id) {
        userRepository.deleteById(id);
        log.info("IN delete - user with id: {} successfully deleted", id);
    }*/

    public boolean isEmailUnique(String email) {
        User user = userRepository.findByEmail(email).orElse(null);
        return user == null;
    }

    public boolean isLoginUnique(String login) {
        User user = userRepository.findByUsername(login).orElse(null);
        return user == null;
    }

    private void valCheckRegistration(RegistrationRequestDto registrationRequest) {
        if (registrationRequest.getEmail().isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Messages.EMAIL_IS_EMPTY);
        }
        if (registrationRequest.getLogin().isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Messages.LOGIN_IS_EMPTY);
        }
        if (registrationRequest.getPassword().isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Messages.PASSWORD_IS_EMPTY);
        }
        if (!isEmailUnique(registrationRequest.getEmail())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Messages.EMAIL_NOT_UNIQUE);
        }
        if (!isLoginUnique(registrationRequest.getLogin())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Messages.LOGIN_NOT_UNIQUE);
        }
    }

    public User getCurrentUser() {
        String currentUserName = SecurityContextHolder.getContext().getAuthentication().getName();
        log.info("current user name : {}", currentUserName);
        User currentUser = userRepository.findByUsername(currentUserName)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                        Messages.USER_NOT_AUTHORIZED));
        log.info("User {} found by token", currentUser);
        return currentUser;
    }

    public UserDto getUserProfile() {
        User user;
        try {
            user = getCurrentUser();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
        return UserDto.fromUser(user);
    }

    public ResponseEntity<?> changeUserProfile(ChangedProfileDto changedProfileDto) {
        User user;
        try {
            user = getCurrentUser();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new ResponseMessage(Messages.USER_NOT_AUTHORIZED));
        }
        ResponseMessage rm = validChangesMessage(user, changedProfileDto);
        if (rm != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(rm);
        }
        user.setUsername(changedProfileDto.getLogin());
        user.setEmail(changedProfileDto.getEmail());
        userRepository.save(user);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(Messages.UPDATE_PROFILE_SUCCESS));
    }

    private ResponseMessage validChangesMessage(User user, ChangedProfileDto changedProfileDto) {
        String username = user.getUsername();
        String userDtoName = changedProfileDto.getLogin();
        String email = user.getEmail();
        String userDtoEmail = changedProfileDto.getEmail();
        if (!username.equals(userDtoName) && !isLoginUnique(userDtoName)) {
            return new ResponseMessage(Messages.LOGIN_NOT_UNIQUE);
        }
        if (!email.equals(userDtoEmail) && !isEmailUnique(userDtoEmail)) {
            return new ResponseMessage(Messages.EMAIL_NOT_UNIQUE);
        }
        if (userDtoEmail.isEmpty() || userDtoEmail.contains(" ")) {
            return new ResponseMessage(Messages.INCORRECT_EMAIL);
        }
        if (userDtoName.isEmpty() || userDtoName.contains(" ")) {
            return new ResponseMessage(Messages.INCORRECT_LOGIN);
        }

        return null;
    }

    public ResponseEntity<?> changePassword(ChangedPasswordDto changedPasswordDto) {
        User user;
        try {
            user = getCurrentUser();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new ResponseMessage(Messages.USER_NOT_AUTHORIZED));
        }
        ResponseMessage rm = valPassChangeMessage(user, changedPasswordDto);
        if (rm != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(rm);
        }
        user.setPasswordHash(passwordEncoder.encode(changedPasswordDto.getNewPassword()));
        userRepository.save(user);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(Messages.UPDATE_PASSWORD_SUCCESS));
    }

    private ResponseMessage valPassChangeMessage(User user, ChangedPasswordDto changedPasswordDto) {
        String userPassword = user.getPasswordHash();
        if (passwordEncoder.matches(changedPasswordDto.getOldPassword(), userPassword)) {
            if (!changedPasswordDto.getNewPassword().isEmpty() && !changedPasswordDto.getNewPassword().contains(" ")) {
                return null;
            }
            return new ResponseMessage(Messages.INCORRECT_NEW_PASSWORD);
        }
        return new ResponseMessage(Messages.INCORRECT_PASSWORD);
    }
}
