package com.qayn.qayn_server.service;

import com.qayn.qayn_server.api.Messages;
import com.qayn.qayn_server.dto.QuoteDto;
import com.qayn.qayn_server.dto.SourceDto;
import com.qayn.qayn_server.model.Quote;
import com.qayn.qayn_server.model.QuoteFrom;
import com.qayn.qayn_server.model.Source;
import com.qayn.qayn_server.model.User;
import com.qayn.qayn_server.repository.QuoteFromRepository;
import com.qayn.qayn_server.repository.QuoteRepository;
import com.qayn.qayn_server.repository.SourceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SourceService {
    private final SourceRepository sourceRepository;
    private final QuoteFromRepository quoteFromRepository;
    private final QuoteRepository quoteRepository;
    private final RatingService ratingService;
    private final UserService userService;

    public SourceDto getSourceById(int id) {
        return new SourceDto(sourceRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        Messages.SOURCE_NOT_FOUND)));
    }

    public List<QuoteDto> quotesFromSource(int sourceId) {
        Source source = sourceRepository.findById(sourceId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        Messages.SOURCE_NOT_FOUND));

        List<Quote> quotes = quoteRepository.findByMainSource(source);
        List<QuoteFrom> quoteFromList = quoteFromRepository.findBySource(source);
        for (QuoteFrom quoteFrom : quoteFromList) {
            quotes.add(quoteFrom.getQuote());
        }
        Collections.sort(quotes);
        List<QuoteDto> result = new ArrayList<>();
        User currentUser = userService.getCurrentUser();
        for (Quote quote : quotes) {
            result.add(new QuoteDto(quote,
                    ratingService.getUserRating(currentUser, quote)));
        }
        return result;
    }
}
