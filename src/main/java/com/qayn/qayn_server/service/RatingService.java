package com.qayn.qayn_server.service;

import com.qayn.qayn_server.dto.QuoteDto;
import com.qayn.qayn_server.dto.UserRatingEnum;
import com.qayn.qayn_server.model.Dislike;
import com.qayn.qayn_server.model.Like;
import com.qayn.qayn_server.model.Quote;
import com.qayn.qayn_server.model.User;
import com.qayn.qayn_server.repository.DislikeRepository;
import com.qayn.qayn_server.repository.LikeRepository;
import com.qayn.qayn_server.repository.QuoteRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class RatingService {
    private final LikeRepository likeRepository;
    private final DislikeRepository dislikeRepository;
    private final QuoteRepository quoteRepository;

    public void removeCurrentRatingIfExists(User user, Quote quote) {
        Like like = likeRepository.findByUserAndQuote(user, quote).orElse(null);

        if (like != null) {
            log.info("delete like for user : {}, quote : {}", user, quote);
            likeRepository.delete(like);
            quote.setRating(quote.getRating() - 1);
            quoteRepository.setRatingById(quote.getId(), quote.getRating());
        }

        Dislike dislike = dislikeRepository.findByUserAndQuote(user, quote).orElse(null);

        if (dislike != null) {
            log.info("delete dislike for user : {}, quote : {}", user, quote);
            dislikeRepository.delete(dislike);
            quote.setRating(quote.getRating() + 1);
            quoteRepository.setRatingById(quote.getId(), quote.getRating());
        }
    }

    public void likeQuote(User user, Quote quote) {
        Like like = new Like();
        like.setUser(user);
        like.setQuote(quote);
        log.info("new like for user : {}, quote : {}", user, quote);
        likeRepository.save(like);
        quote.setRating(quote.getRating() + 1);
        quoteRepository.setRatingById(quote.getId(), quote.getRating());
    }

    public void dislikeQuote(User user, Quote quote) {
        Dislike dislike = new Dislike();
        dislike.setUser(user);
        dislike.setQuote(quote);
        log.info("new dislike for user : {}, quote : {}", user, quote);
        dislikeRepository.save(dislike);
        quote.setRating(quote.getRating() - 1);
        quoteRepository.setRatingById(quote.getId(), quote.getRating());
    }

    public List<QuoteDto> collection(User user) {
        List<Like> likes = likeRepository.findByUser(user);
        List<QuoteDto> quotes = new ArrayList<>();
        for (Like like : likes) {
            quotes.add(new QuoteDto(like.getQuote(), UserRatingEnum.LIKE));
        }
        return quotes;
    }

    public UserRatingEnum getUserRating(User user, Quote quote) {
        Like like = likeRepository.findByUserAndQuote(user, quote).orElse(null);
        if (like != null) {
            return UserRatingEnum.LIKE;
        }

        Dislike dislike = dislikeRepository.findByUserAndQuote(user, quote).orElse(null);
        if (dislike != null) {
            return UserRatingEnum.DISLIKE;
        }

        return UserRatingEnum.NONE;
    }
}
