package com.qayn.qayn_server.security.jwt;

import com.qayn.qayn_server.model.User;
import com.qayn.qayn_server.model.UserRole;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public final class JwtUserFactory {

    public JwtUserFactory() {

    }

    public static JwtUser create(User user) {
        return new JwtUser(
                user.getId(),
                user.getUsername(),
                user.getEmail(),
                user.getPasswordHash(),
                user.getRole(),
                mapToGrantedAuthorities(new ArrayList<UserRole>(Arrays.asList(user.getRole()))),
                true
        );
    }

    private static List<GrantedAuthority> mapToGrantedAuthorities(List<UserRole> roles) {
        return roles.stream()
                .map(userRole ->
                        new SimpleGrantedAuthority(userRole.getRoleName())
                ).collect(Collectors.toList());
    }

}
