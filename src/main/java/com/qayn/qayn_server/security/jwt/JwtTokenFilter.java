package com.qayn.qayn_server.security.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qayn.qayn_server.api.Messages;
import com.qayn.qayn_server.api.ResponseMessage;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtTokenFilter extends GenericFilterBean {

    private JwtTokenProvider jwtTokenProvider;

    public JwtTokenFilter(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException {
        try {
            String token = jwtTokenProvider.resolveToken((HttpServletRequest) servletRequest);

            if (token != null && jwtTokenProvider.validateToken(token)) {
                Authentication authentication = jwtTokenProvider.getAuthentication(token);

                if (authentication != null) {
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }
            filterChain.doFilter(servletRequest, servletResponse);
        } catch (Exception e) {
            servletResponse.setCharacterEncoding("UTF-8");
            servletResponse.setContentType("text/html; charset=UTF-8");
            HttpServletResponse httpResp = (HttpServletResponse)servletResponse;
            httpResp.setStatus(HttpStatus.UNAUTHORIZED.value());
            ResponseMessage msg = new ResponseMessage(Messages.USER_NOT_AUTHORIZED);
            String serialized = new ObjectMapper().writeValueAsString(msg);
            servletResponse.getWriter().print(serialized);
        }
    }
}
